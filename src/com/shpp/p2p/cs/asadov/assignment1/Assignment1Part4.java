package com.shpp.p2p.cs.asadov.assignment1;
/* File: Assignment1Part4.java
 *
 * In this program Karel makes chessboard.
 */

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part4 extends KarelTheRobot {

    /* Before: Karel stands in the cell in the west-south corner facing east.
     * After: Karel stands in the cell in north-west or north-east corner.
     *        There are beepers in cells in chess order.
     */
    public void run() throws Exception {
        // put first beeper in south-west corner
        putBeeper();
        // while Karel not in north-west or north-east corner
        while (!frontIsBlocked() ||
                !((rightIsBlocked() && facingWest()) || (leftIsBlocked() && facingEast()))) {
            // Karel walks on desk as on one line and puts beeper on every even step
            makeOddAndEvenSteps();
        }
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel stands in next cell looking same direction
     *        or in cell of next line looking opposite direction.
     */
    private void makeOddAndEvenSteps() throws Exception {
        // Karel makes odd step
        makeStep();
        // if in north-west or north-east corner doesn't make any steps
        if (frontIsBlocked() &&
                ((rightIsBlocked() && facingWest()) || (leftIsBlocked() && facingEast()))) {
            return;
        }
        // Karel makes even step...
        makeStep();
        // ...and on even step puts beeper
        putBeeper();
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel stands in next cell looking same direction
     *        or in cell of next line looking opposite direction.
     */
    private void makeStep() throws Exception {
        // Then Karel meets wall, step means not 'move' but 'turn and shift'
        if (frontIsClear()) {
            move();
        } else {
            moveToNextLineWithTurn();
        }
    }

    /* Before: Karel stands in one cell facing east or west.
     * After: Karel moves to the next line left or right depends on his facing
     *        and stands looking in opposite direction.
     */
    private void moveToNextLineWithTurn() throws Exception {
        if (facingEast()) {
            // odd lines ends
            turnAndShiftLeft();
        } else {
            // even lines ends
            turnAndShiftRight();
        }
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel shifts one cell to the left
     *        and stands looking in opposite direction.
     */
    private void turnAndShiftLeft() throws Exception {
        turnLeft();
        move();
        turnLeft();
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel shifts one cell to the right
     *        and stands looking in opposite direction.
     */
    private void turnAndShiftRight() throws Exception {
        turnRight();
        move();
        turnRight();
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel stands in the same cell looking 90 degree clockwise.
     */
    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }
}