package com.shpp.p2p.cs.asadov.assignment1;


import com.shpp.karel.KarelTheRobot;

public class Assignment1Part3bk extends KarelTheRobot {

    public void run() throws Exception {
        if (frontIsClear()) {
            putBeeperOnEveryCell();
            turnAround();
            collectBeepersInFirstCell();
            turnAround();
            removeBeeprsToNextCall();
        }
        putBeeper();
    }


    private void removeBeeprsToNextCall() throws Exception {

        while (beepersPresent()) {
            pickBeeper();
            if (beepersPresent()) {
                pickBeeper();
            }
            while (beepersPresent()) {
                pickBeeper();
                move();
                putBeeper();
                turnAround();
                move();
                turnAround();

            }
            move();
        }

    }


    private void collectBeepersInFirstCell() throws Exception {
        while (frontIsClear()) {
            while (beepersPresent()) {
                pickBeeper();
                move();
                putBeeper();
                turnAround();
                move();
                turnAround();
            }
            move();
        }
    }

    private void putBeeperOnEveryCell() throws Exception {
        while (frontIsClear()) {
            move();
            putBeeper();
        }
    }

    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }
}
