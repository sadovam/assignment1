package com.shpp.p2p.cs.asadov.assignment1;

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part4b extends KarelTheRobot {

    public void run() throws Exception {
        putBeeper();
        while (!(
                frontIsBlocked() &&
                        ((rightIsBlocked() && facingWest()) || (leftIsBlocked() && facingEast()))
        ) ){
            makeLine();
        }

        повернути();
        if(noBeepersPresent()) {
            move();
        }

        while (!(
                frontIsBlocked() &&
                        ((rightIsBlocked() && facingEast()) || (leftIsBlocked() && facingWest()))
        ) ){
            makeLine2();
        }
    }

    private void makeLine() throws Exception {
        makeStep();
        if (!(
                frontIsBlocked() &&
                        ((rightIsBlocked() && facingWest()) || (leftIsBlocked() && facingEast()))
        ) ){
            makeStep();
            putBeeper();
        }

    }

    private void makeLine2() throws Exception {
        makeStep2();
        if (!(
                frontIsBlocked() &&
                        ((rightIsBlocked() && facingEast()) || (leftIsBlocked() && facingWest()))
        ) ){
            makeStep2();
            if (noBeepersPresent()) {
                putBeeper();
            }
        }

    }

    private void makeTurn() throws Exception {
        if (facingEast()) {
            turnLeft();
            move();
            turnLeft();
        } else {
            turnRight();
            move();
            turnRight();
        }
    }

    private void makeTurn2() throws Exception {
        if (facingWest()) {
            turnLeft();
            move();
            turnLeft();
        } else {
            turnRight();
            move();
            turnRight();
        }
    }

    private void makeStep() throws Exception {
        if (frontIsClear()) {
            move();
        } else {
            makeTurn();
        }
    }

    private void makeStep2() throws Exception {
        if (frontIsClear()) {
            move();
        } else {
            makeTurn2();
        }
    }


    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }

    private void повернути() throws Exception {
        turnLeft();
        turnLeft();
    }

}