package com.shpp.p2p.cs.asadov.assignment1;
/* File: Assignment1Part1.java
 *
 * In this program Karel takes the newspaper.
 */

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part1 extends KarelTheRobot {

    /* Before: Karel stands in north-west corner of his house looks east.
     * After: Karel stands in north-west corner of his house looks east. (with the newspaper :))
     */
    public void run() throws Exception {
        goForNewspaper();
        takeNewspaper();
        goHome();
    }

    /* Before: Karel stands in north-west corner of his house looks east.
     * After: Karel stands in cell with the newspaper looks west.
     */
    private void goForNewspaper() throws Exception {
        shiftRight();
        moveFourCells();
        turnAround();
    }

    /* Before: Karel stands in cell with the newspaper looks west.
     * After: Karel stands in cell with the newspaper looks west. (with the newspaper :))
     * This method not necessary for logic, added only for semantic.
     */
    private void takeNewspaper() throws Exception {
        pickBeeper();
    }

    /* Before: Karel stands in cell with the newspaper faces west.
     * After: Karel stands in north-west corner of his house faces east. (with the newspaper :))
     */
    private void goHome() throws Exception {
        moveFourCells();
        shiftRight();
        turnAround();
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel moves four cells forward and stands looking in same direction.
     */
    private void moveFourCells() throws Exception {
        for (int i = 0; i < 4; i++) {
            move();
        }
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel shifts one cell to the right and stands looking in same direction.
     */
    private void shiftRight() throws Exception {
        turnRight();
        move();
        turnLeft();
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel stands in the same cell looking 90 degree clockwise.
     */
    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel stands in the same cell looking in opposite direction.
     */
    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }
}