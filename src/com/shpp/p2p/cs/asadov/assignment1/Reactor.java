package com.shpp.p2p.cs.asadov.assignment1;


import com.shpp.karel.KarelTheRobot;

public class Reactor extends KarelTheRobot {

    public void run() throws Exception {
        clearSection();
        if (frontIsClear()) {
            move();
            move();
            run();

        }

    }

    private void clearSection() throws Exception {
        if (noBeepersPresent()) {
            turnLeft();
            comeInToCellAndClearIt();
            comeInToCellAndClearIt();
            turnRight();
        }

    }

    private void comeInToCellAndClearIt() throws Exception {
        move();
        clearCell();
        turnAround();
        move();
    }

    private void clearCell() throws Exception {
        while(beepersPresent()) {
            pickBeeper();
        }
    }

    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }

    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }
}
