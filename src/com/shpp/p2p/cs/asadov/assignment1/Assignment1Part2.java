package com.shpp.p2p.cs.asadov.assignment1;
/* File: Assignment1Part2.java
 *
 * In this program Karel builds columns.
 */

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part2 extends KarelTheRobot {

    /* Before: Karel stands in the cell in the west-south corner facing east.
     *         To his left is the place for of column. To his right is the wall.
     * After: Karel stands in the cell in the east-south corner facing east.
     *        All columns of beepers are complete.
     */
    public void run() throws Exception {
        // Karel builds first and possibly the only column.
        buildColumn();
        // If column not only one, Karel walks to next places for column and builds others.
        while (frontIsClear()) {
            moveFourCells();
            buildColumn();
        }
    }


    /* Before: Karel stands in the cell facing east. To his left is the place for of column.
               To his right is the wall.
     * After: Karel stands in the same cell looking in same direction.
     *        To his left is complete column of beepers.
     */
    private void buildColumn() throws Exception {
        turnLeft();
        makeLine();
        turnAround();
        goToWall();
        turnLeft();
    }

    /* Before: Karel stands in the cell at the begin of column facing north (towards the wall).
     * After: Karel stands in the cell looking in same direction and touching the wall.
     *        Behind him is complete column of beepers.
     */
    private void makeLine() throws Exception {
        // Karel makes line of stones (beepers)
        while (frontIsClear()) {
            putBeeperIfNotPresent();
            move();
        }
        // put the last beeper near the wall
        putBeeperIfNotPresent();
    }

    /* Before: Karel stands in one cell. In the cell 1 or 0 beepers.
     * After: Karel stands in the same cell. In the cell 1 beeper.
     */
    private void putBeeperIfNotPresent() throws Exception {
        // Karel puts beeper in the cell only if it's empty.
        if (noBeepersPresent()) {
            putBeeper();
        }
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel stands in the cell looking in same direction and touching to the wall.
     */
    private void goToWall() throws Exception {
        // Karel move forward until touching to the wall.
        while (frontIsClear()) {
            move();
        }
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel moves four cells forward and stands looking in same direction.
     */
    private void moveFourCells() throws Exception {
        for (int i = 0; i < 4; i++) {
            move();
        }
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel stands in the same cell looking in opposite direction.
     */
    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }
}
