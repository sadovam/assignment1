package com.shpp.p2p.cs.asadov.assignment1;
/* File: Assignment1Part3.java
 *
 * In this program Karel finds midpoint.
 */

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part3 extends KarelTheRobot {

    /* Before: Karel stands in the cell in the west-south corner facing east.
     * After: Karel stands in the cell in the middle of south line.
     *        There are no beepers in cells except only one in this cell.
     */
    public void run() throws Exception {
        // If cell is the only one Karel just put beeper.
        if (frontIsBlocked()) {
            putBeeper();
        } else {
            // Karel puts beepers in every cell in south line except first and last cells.
            makeLineOfBeepersWithoutFirstAndLast();
            // For the starts moving Karel may be in non-empty cell.
            turnAround();
            move();
            // Then Karel picks beepers from opposite sides one by one.
            while (beepersPresent()) {
                trimEdgeBeeper();
            }
            // Than beepers ends, Karel returns to cell there was the last one
            // and puts beeper here.
            putBeeperInBackCell();
        }
    }

    /* Before: Karel stands in the first cell of line looking towards the opposite wall.
     * After: Karel stands in the last cell of line looking in the same direction.
     *        There are beepers in all cells of line except first and last.
     */
    private void makeLineOfBeepersWithoutFirstAndLast() throws Exception {
        // Karel put beepers in all cells of line except first...
        while (frontIsClear()) {
            move();
            putBeeper();
        }
        // and picks the last.
        pickBeeper();
    }

    /* Before: Karel stands in the first non-empty cell of line
     *         looking towards the opposite wall.
     * After: Karel turned around and stands in the last non-empty cell
     *        near the opposite wall after picking one beeper from this edge.
     */
    private void trimEdgeBeeper() throws Exception {
        // Karel walks before finds empty cell
        while (beepersPresent()) {
            move();
        }
        // then returns to last non-empty
        turnAround();
        move();
        // picks beeper and move to new last non-empty cell
        pickBeeper();
        move();
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel stands in the back cell this beeper.
     */
    private void putBeeperInBackCell() throws Exception {
        turnAround();
        move();
        putBeeper();
    }

    /* Before: Karel stands in one cell looking anywhere.
     * After: Karel stands in the same cell looking in opposite direction.
     */
    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }
}
