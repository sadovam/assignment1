package com.shpp.p2p.cs.asadov.assignment1;


import com.shpp.karel.KarelTheRobot;

public class Assignment1Part3a extends KarelTheRobot {

    public void run() throws Exception {
        if (frontIsClear()) {
            makeDiagonal();
            turnAround();
            goUntilNotWall();
            turnAround();
            makeDiagonal2();
            turnRight();
            goUntilNotWall();
            putBeeper();
            turnRight();
            goUntilNotWall();
            turnAround();
            makeDiagonal3();
        } else {
            putBeeper();
        }
        //putBeeper();
    }

    private void makeDiagonal3() throws Exception {
        while (frontIsClear()) {
            move();
            turnLeft();
            move();
            turnRight();
            pickBeeper();

        }
    }


    private void makeDiagonal2() throws Exception {
        while (noBeepersPresent()) {
            move();

            if(noBeepersPresent()) {
                turnRight();
                move();
                turnLeft();
            }
        }
    }

    private void makeDiagonal() throws Exception {
        while (frontIsClear()) {
            move();
            turnLeft();
            move();
            turnRight();
            putBeeper();
        }
    }

    private void goUntilNotWall() throws Exception {
        while(frontIsClear()){
            move();
        }
    }

    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }

    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }
}
