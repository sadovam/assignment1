package com.shpp.p2p.cs.asadov.assignment1;

import com.shpp.karel.KarelTheRobot;

public class Assignment1Part4bk extends KarelTheRobot {

    public void run() throws Exception {
        makeOddLine();
        goBack();
        while (rightIsClear()) {
            turnRight();
            move();
            turnRight();
            makeEvanLine();
            goBack();
            if (rightIsClear()) {
                turnRight();
                move();
                turnRight();
                makeOddLine();
                goBack();
            }
        }
    }

    private void makeOddLine() throws Exception {
        putBeeper();
        while (frontIsClear()) {

            move();
            if (frontIsClear()) {
                move();
                putBeeper();
            }

        }
    }

    private void makeEvanLine() throws Exception {
        while (frontIsClear()) {

            move();
            putBeeper();
            if (frontIsClear()) {

                move();

            }

        }
    }

    private void goBack() throws Exception {
        turnAround();
        while (frontIsClear()) {
            move();
        }

    }

    private void goHome() throws Exception {
        turnAround();
        move();
        move();
        move();
        move();
        turnRight();
        move();
        turnRight();
    }

    private void turnRight() throws Exception {
        for (int i = 0; i < 3; i++) {
            turnLeft();
        }
    }

    private void turnAround() throws Exception {
        turnLeft();
        turnLeft();
    }

}